<?php

/**
 * 
 * A simple wrapper for the TwitterOAuth package
 * 
 * Version: 0.1
 * _______________
 * By Rory O'Brien
 */

require "twitteroauth/autoload.php";

use Abraham\TwitterOAuth\TwitterOAuth;

class Twitter
{
    public function __construct($api_key, $api_secret, $access_token, $access_token_secret)
    {
        $this->connection = new TwitterOAuth($api_key, $api_secret, $access_token, $access_token_secret);
    }

    public function getUserTweets($amount, $replies)
    {
        if (filter_var($amount, FILTER_VALIDATE_INT)) {
            return $this->connection->get("statuses/user_timeline", ["count" => $amount, "exclude_replies" => $replies]);
        } else {
            die("Invalid argument passed to getUserTweets, must be an integer");
        }
    }
}
